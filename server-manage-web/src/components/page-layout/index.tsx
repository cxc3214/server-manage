import React, {useState} from "react";
import {Outlet, useNavigate} from "react-router-dom";
import { Layout, Nav, Button, Breadcrumb, Skeleton, Avatar } from '@douyinfe/semi-ui';
import { IconBell, IconHelpCircle, IconMoon, IconHome, IconHistogram, IconLive, IconSetting, IconSun } from '@douyinfe/semi-icons';

export const PageLayout = () => {

    const [mode, setMode] = useState('semi-always-light');
    const navigate = useNavigate();

    const switchMode = () => {
        const newMode = mode === 'semi-always-dark' ? 'semi-always-light' : 'semi-always-dark';
        setMode(newMode);
    };

    const pageJump = (key: string, isOpen: boolean) => {
        console.log("key", key, "open", isOpen)
        navigate(key)
    }

    return <Layout className={mode} style={{ height: '100vh' }}>
        <Layout.Sider style={{ backgroundColor: 'var(--semi-color-bg-1)' }}>
            <Nav
                onClick={e => pageJump(e.itemKey as string, e.isOpen)}
                defaultSelectedKeys={['Home']}
                style={{ maxWidth: 220, height: '100%' }}
                items={[
                    { itemKey: '/manage/server', text: '服务器', icon: <IconHome size="large" /> },
                    { itemKey: '/manage/docker', text: '容器管理', icon: <IconHistogram size="large" /> },
                    { itemKey: '/manage/script', text: '脚本管理', icon: <IconLive size="large" /> },
                    { itemKey: '/manage/task', text: '定时任务', icon: <IconLive size="large" /> },
                    { itemKey: '/manage/setting', text: '设置', icon: <IconSetting size="large" /> },
                ]}
                header={{
                    logo: <img src="//lf1-cdn-tos.bytescm.com/obj/ttfe/ies/semi/webcast_logo.svg" />,
                    text: '服务管理平台',
                }}
                footer={{
                    collapseButton: true,
                }}
            />
        </Layout.Sider>
        <Layout>
            <Layout.Header style={{ backgroundColor: 'var(--semi-color-bg-1)' }}>
                <Nav
                    mode="horizontal"
                    footer={
                        <>
                            <Button
                                theme='borderless'
                                icon={mode === 'semi-always-light' ? <IconMoon /> : <IconSun />}
                                style={{
                                    marginRight: '12px',
                                    color: 'var(--semi-color-text-2)',
                                }}
                                onClick={switchMode}
                            ></Button>
                            <Button
                                theme="borderless"
                                icon={<IconBell size="large" />}
                                style={{
                                    color: 'var(--semi-color-text-2)',
                                    marginRight: '12px',
                                }}
                            />
                            <Button
                                theme="borderless"
                                icon={<IconHelpCircle size="large" />}
                                style={{
                                    color: 'var(--semi-color-text-2)',
                                    marginRight: '12px',
                                }}
                            />
                            <Avatar color="orange" size="small">
                                麒麟
                            </Avatar>
                        </>
                    }
                ></Nav>
            </Layout.Header>
            <Layout.Content style={{padding: '24px', backgroundColor: 'var(--semi-color-bg-0)'}}>
                <Outlet />
            </Layout.Content>
            <Layout.Footer
                style={{
                    display: 'flex',
                    justifyContent: 'space-between',
                    padding: '20px',
                    color: 'var(--semi-color-text-2)',
                    backgroundColor: 'rgba(var(--semi-grey-0), 1)',
                }}
            >
                    <span
                        style={{
                            display: 'flex',
                            alignItems: 'center',
                        }}
                    >
                        {/*<IconBytedanceLogo size="large" style={{ marginRight: '8px' }} />*/}
                        {/*<span>Copyright © 2019 ByteDance. All Rights Reserved. </span>*/}
                    </span>
                    <span>
                        <span>反馈建议</span>
                    </span>
            </Layout.Footer>
        </Layout>
    </Layout>

}