import React from "react";
import {Navigate, useRoutes} from "react-router-dom";
import {PageLayout} from "../components/page-layout";
import {LoginPage} from "../pages/login";
import {ScriptPage} from "../pages/script";
import {ServerPage} from "../pages/server";
import {DockerPage} from "../pages/docker";
import {TaskPage} from "../pages/task";
import {SettingPage} from "../pages/setting";

export const SystemRouter = (): React.ReactElement | null => {
    return useRoutes([
        {
            path: '*',
            element: <Navigate to={"/manage/login"} />,
        },
        {
            path: 'manage/login',
            element: <LoginPage />,
        },
        {
            path: "manage",
            element: <PageLayout />,
            children: [
                {
                    path: "server",
                    element: <ServerPage />
                },
                {
                    path: "docker",
                    element: <DockerPage />
                },
                {
                    path: 'task',
                    element: <TaskPage />
                },
                {
                    path: 'script',
                    element: <ScriptPage />
                },
                {
                    path: 'setting',
                    element: <SettingPage />
                }
            ]
        }
    ])
}