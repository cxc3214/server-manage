import React from "react";
import {BrowserRouter} from "react-router-dom";
import {PersistGate} from 'redux-persist/lib/integration/react';
import {Provider} from "react-redux";
import {persist, store} from "../redux";
import {SystemRouter} from "../router";

export const Application = () => {

    return <Provider store={store}>
        <PersistGate loading={null} persistor={persist}>
            <BrowserRouter>
                <SystemRouter />
            </BrowserRouter>
        </PersistGate>
    </Provider>

}