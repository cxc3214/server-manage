import {CHANGE_LONGIN_STATUS, UserTypes} from './action';

export interface UserState {
    // 登陆状态
    status: boolean;
    token: string;
    roleName: string;
    username: string;
    id?: number;
}

const defaultState: UserState = {
    status: true,
    token: '',
    roleName: '',
    username: '',
}

export default function userReducer(state = defaultState, action: UserTypes) {
    switch (action.type) {
        case CHANGE_LONGIN_STATUS:
            return {
                ...state,
                ...action.payload
            };
        default:
            return state;
    }
}