package meblog.online.server.message;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serial;
import java.io.Serializable;

@Data
@Accessors(chain = true)
public class UserLoginVo implements Serializable {

    @Serial
    private static final long serialVersionUID = -6952455532797533437L;

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

}
