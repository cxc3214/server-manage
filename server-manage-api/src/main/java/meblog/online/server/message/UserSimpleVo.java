package meblog.online.server.message;

import lombok.Data;
import lombok.experimental.Accessors;
import meblog.online.server.entity.User;

import java.io.Serial;
import java.io.Serializable;

@Data
@Accessors(chain = true)
public class UserSimpleVo implements Serializable {

    @Serial
    private static final long serialVersionUID = 2006386221041902265L;

    private Integer id;

    private String username;

    private String roleName;

    private String token;

    public static UserSimpleVo build(User user) {
        return new UserSimpleVo()
                .setId(user.getId())
                .setUsername(user.getUsername())
                .setRoleName(UserRoleEnum.name(user.getRoleType()));
    }

}
