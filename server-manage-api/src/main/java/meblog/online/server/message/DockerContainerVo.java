package meblog.online.server.message;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Accessors(chain = true)
public class DockerContainerVo implements Serializable {

    @Serial
    private static final long serialVersionUID = -7818742587409796014L;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 容器ID
     */
    private String id;

    /**
     * 镜像
     */
    private String image;

    /**
     * 镜像ID
     */
    private String imageId;

    /**
     * 容器名称
     */
    private String containerName;

    /**
     * 状态
     */
    private String status;

    /**
     * 状态值
     */
    private String state;

}
