package meblog.online.server.message;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serial;
import java.io.Serializable;

@Data
@Accessors(chain = true)
public class ServerUpdateVo extends ServerInsertVo implements Serializable {
    @Serial
    private static final long serialVersionUID = -5164578467723806126L;

    /**
     * 主键
     */
    @NotNull(message = "数据标识不存在")
    private Integer id;

}
