package meblog.online.server.mapper;

import meblog.online.server.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
* @author yuelong
* @description 针对表【m_user(系统用户)】的数据库操作Mapper
* @createDate 2022-11-22 17:05:50
* @Entity meblog.online.server.entity.User
*/
@Mapper
public interface UserMapper extends BaseMapper<User> {

}




