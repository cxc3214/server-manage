package meblog.online.server.websocket;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import lombok.extern.slf4j.Slf4j;
import meblog.online.server.service.ChannelService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Slf4j
@Component
@ChannelHandler.Sharable
public class WsHeartBeatHandler  extends ChannelInboundHandlerAdapter {

    @Resource
    private ChannelService channelService;

    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
        if (evt instanceof IdleStateEvent event) {
            if (event.state() == IdleState.READER_IDLE) {
                log.debug("没有收到读数据包");
            } else if (event.state() == IdleState.WRITER_IDLE) {
                log.debug("没有发送写数据包");
            } else if (event.state() == IdleState.ALL_IDLE) {
                Channel channel = ctx.channel();
                log.error("长时间没有读写，关闭连接: {}", channel.id().asLongText());
                channelService.remove(channel);
                channel.close();
            }
        }
    }

}
