package meblog.online.server.websocket;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpContentCompressor;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;
import io.netty.handler.stream.ChunkedWriteHandler;
import io.netty.handler.timeout.IdleStateHandler;
import meblog.online.server.service.ChannelService;
import meblog.online.server.utils.SpringUtil;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

@Component
public class WSChannelInitializer extends ChannelInitializer<SocketChannel> {

    private final ClientMsgHandler clientMsgHandler;
    private final WsHeartBeatHandler heartBeatHandler;

    public WSChannelInitializer() {
        clientMsgHandler = SpringUtil.getBean(ClientMsgHandler.class);
        heartBeatHandler = SpringUtil.getBean(WsHeartBeatHandler.class);
    }


    @Override
    protected void initChannel(@NotNull SocketChannel ch) throws Exception {
        ChannelPipeline pipeline = ch.pipeline();
        // http编解码器
        pipeline.addLast(new HttpServerCodec());
        // 块写入
        pipeline.addLast(new ChunkedWriteHandler());
        // 将请求报文聚合为完整报文，设置最大请求报文 10M
        pipeline.addLast(new HttpObjectAggregator(10 * 1024 * 1024));
        // 心跳
        pipeline.addLast(new IdleStateHandler(10, 10, 30, TimeUnit.MINUTES));
        // 处理心跳
        pipeline.addLast(heartBeatHandler);
        // 处理ws信息
        pipeline.addLast(new WebSocketServerProtocolHandler("/api/ws"));
        pipeline.addLast(clientMsgHandler);
    }
}
