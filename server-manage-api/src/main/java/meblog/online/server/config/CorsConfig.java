package meblog.online.server.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

/**
 * @author: Eternity.麒麟
 * @description: 跨域配置
 * @date: 2022/3/8 23:06
 * @version: 1.0
 */
@Slf4j
@Configuration
public class CorsConfig {


    @Bean
    public CorsFilter corsFilter() {
        log.info("设置跨域");
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        corsConfiguration.setAllowCredentials(true);
        // 允许任何头
        corsConfiguration.addAllowedHeader("*");
        // 允许任何方法（post、get等）
        corsConfiguration.addAllowedMethod("*");
        //
        corsConfiguration.addExposedHeader("*");
        corsConfiguration.addAllowedOrigin("http://127.0.0.1:5173");
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        // 对接口配置跨域设置
        source.registerCorsConfiguration("/**", corsConfiguration);
        return new CorsFilter(source);
    }


}
