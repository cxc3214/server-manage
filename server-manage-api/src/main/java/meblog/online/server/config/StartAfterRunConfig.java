package meblog.online.server.config;

import meblog.online.server.websocket.WebSocketServer;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 项目启动之后执行
 */
@Component
public class StartAfterRunConfig implements CommandLineRunner {

    @Resource
    private WebSocketServer webSocketServer;

    @Override
    public void run(String... args) throws Exception {
        webSocketServer.Run();
    }
}
