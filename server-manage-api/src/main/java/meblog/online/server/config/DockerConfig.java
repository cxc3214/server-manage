package meblog.online.server.config;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.core.DefaultDockerClientConfig;
import com.github.dockerjava.core.DockerClientConfig;
import com.github.dockerjava.core.DockerClientImpl;
import com.github.dockerjava.httpclient5.ApacheDockerHttpClient;
import com.github.dockerjava.transport.DockerHttpClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DockerConfig {

    @Value("${spring.docker.host}")
    private String host;

    @Bean
    public DockerClient dockerClient() {
        DockerClientConfig dockerClientConfig = DefaultDockerClientConfig.createDefaultConfigBuilder()
                .withDockerHost(host)
//                .withRegistryUsername(HARBOR_USERNAME)
//                .withRegistryPassword(HARBOR_PASSWORD)
//                .withRegistryUrl(HARBOR_LOGIN_ADDRESS)
//                .withDockerTlsVerify(true)
//                .withDockerCertPath(DOCKER_CERT_PATH)
                .build();

        // 配置docker客户端连接及证书信息
//        SSLConfig sslConfig = new LocalDirectorySSLConfig(DOCKER_CERT_PATH);
        DockerHttpClient httpClient = new ApacheDockerHttpClient.Builder()
                .dockerHost(dockerClientConfig.getDockerHost())
//                .sslConfig(sslConfig)
                .build();

        // 目前getInstance方法中，只有此方法被推荐
        return DockerClientImpl.getInstance(dockerClientConfig, httpClient);
    }


}
