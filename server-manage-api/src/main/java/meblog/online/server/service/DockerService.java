package meblog.online.server.service;

import com.github.dockerjava.api.model.Container;

import java.util.List;

public interface DockerService {

    /**
     * 获取容器列表
     * @return
     */
    List<Container> containers();

}
