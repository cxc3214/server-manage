package meblog.online.server.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import meblog.online.server.config.SystemException;
import meblog.online.server.entity.User;
import meblog.online.server.message.UserSimpleVo;
import meblog.online.server.utils.JwtUtil;
import meblog.online.server.utils.ResultCode;
import meblog.online.server.message.UserLoginVo;
import meblog.online.server.service.UserService;
import meblog.online.server.mapper.UserMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Objects;

/**
* @author yuelong
* @description 针对表【m_user(系统用户)】的数据库操作Service实现
* @createDate 2022-11-22 17:05:50
*/
@Service
public class UserServiceImpl implements UserService{

    @Resource
    private UserMapper userMapper;

    @Override
    public UserSimpleVo login(UserLoginVo userLoginVo) {
        User user = userMapper.selectOne(new LambdaQueryWrapper<User>()
                .eq(User::getUsername, userLoginVo.getUsername())
                .eq(User::getCanView, true)
        );
        if (Objects.isNull(user)) {
            throw new SystemException(ResultCode.USER_NOT_EXIST);
        }
        if (!user.getPassword().equals(userLoginVo.getPassword())) {
            throw new SystemException(ResultCode.USER_IDENTIFICATION);
        }
        // 创建token
        String token = JwtUtil.createToken(user);
        return UserSimpleVo.build(user).setToken(token);
    }
}




