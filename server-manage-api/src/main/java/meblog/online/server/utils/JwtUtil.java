package meblog.online.server.utils;

import com.alibaba.fastjson2.JSONObject;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import lombok.extern.slf4j.Slf4j;
import meblog.online.server.config.SystemException;
import meblog.online.server.entity.User;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class JwtUtil {

    /**
     * 过期时间
     */
    private static final Long EXPIRATION_DAY = 1L;

    /**
     * 秘钥
     */
    private static final String SECRET = "server-manage";

    /**
     * 生成用户token,设置token超时时间
     */
    public static String createToken(User user){
        //过期时间
        Map<String, Object> map = new HashMap<>();
        map.put("alg", "HS256");
        map.put("typ", "JWT");
        return JWT.create()
                // 添加头部
                .withHeader(map)
                // 数据
                .withClaim("data", JSONObject.toJSONString(user))
                // 超时设置,设置过期的日期
                .withExpiresAt(LocalDateTime.now().plusDays(EXPIRATION_DAY).toInstant(ZoneOffset.ofHours(8)))
                // 签发时间
                .withIssuedAt(LocalDateTime.now().toInstant(ZoneOffset.ofHours(8)))
                // SECRET加密
                .sign(Algorithm.HMAC256(SECRET));
    }

    /**
     * 验证
     * @param token
     * @return
     */
    public static DecodedJWT verifyToken(String token) {
        JWTVerifier verifier = JWT.require(Algorithm.HMAC256(SECRET)).build();
        DecodedJWT jwt = null;
        try {
            jwt = verifier.verify(token);
        } catch (JWTDecodeException e) {
            throw new SystemException(ResultCode.JWT_PARSE_FAIL);
        } catch (TokenExpiredException e) {
            throw new SystemException(ResultCode.AUTH_TIME_EXPIRED);
        }
        return jwt;
    }




    public static void main(String[] args) {
//        User user = new User().setId(1)
//                .setUsername("admin")
//                .setPassword("123456")
//                .setRoleType(1)
//                .setCreateTime(LocalDateTime.now())
//                .setUpdateTime(LocalDateTime.now())
//                .setCanView(true);
//        String token = JwtUtil.createToken(user);
//        log.info("token: {}", token);

        String token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjoie1wiY2FuVmlld1wiOnRydWUsXCJjcmVhdGVUaW1lXCI6XCIyMDIyLTExLTIyIDE4OjM1OjAzLjA2MTQzM1wiLFwiaWRcIjoxLFwicGFzc3dvcmRcIjpcIjEyMzQ1NlwiLFwicm9sZVR5cGVcIjoxLFwidXBkYXRlVGltZVwiOlwiMjAyMi0xMS0yMiAxODozNTowMy4wNjE0NjJcIixcInVzZXJuYW1lXCI6XCJhZG1pblwifSIsImV4cCI6MTY2OTExMzMwNSwiaWF0IjoxNjY5MTEzMzAzfQ.PaJOV8b2x-NnTz0gTmFWeuEaLG4Dv-H7gpyD5NSKegU";
        JwtUtil.verifyToken(token);
    }

}
