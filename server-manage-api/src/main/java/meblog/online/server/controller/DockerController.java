package meblog.online.server.controller;

import com.github.dockerjava.api.model.Container;
import meblog.online.server.service.DockerService;
import meblog.online.server.utils.ResultCode;
import meblog.online.server.utils.ResultMessage;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("docker")
public class DockerController {

    private final DockerService dockerService;

    public DockerController(DockerService dockerService) {
        this.dockerService = dockerService;
    }

    /**
     * 获取容器列表
     * https://docs.docker.com/engine/api/v1.41/#tag/Container/operation/ContainerList
     * @return
     */
    @GetMapping("containers/list")
    public ResultMessage<List<Container>> containers() {
        return new ResultMessage<>(ResultCode.SUCCESS, dockerService.containers());
    }

}
