package meblog.online.server.controller;

import meblog.online.server.config.SecurityUserContext;
import meblog.online.server.entity.User;

public abstract class CommonController {

    public User getUser() {
        return SecurityUserContext.getUserInfo();
    }

}
