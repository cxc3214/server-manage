package meblog.online.server.controller;

import lombok.extern.slf4j.Slf4j;
import meblog.online.server.message.UserSimpleVo;
import meblog.online.server.utils.ResultCode;
import meblog.online.server.utils.ResultMessage;
import meblog.online.server.message.UserLoginVo;
import meblog.online.server.service.UserService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("login")
public class LoginController extends CommonController {

    private final UserService userService;

    public LoginController(UserService userService) {
        this.userService = userService;
    }

    /**
     * 登陆
     * @param userLoginVo
     * @return
     */
    @PostMapping("index")
    public ResultMessage<UserSimpleVo> login(@RequestBody UserLoginVo userLoginVo) {
        return new ResultMessage<>(ResultCode.SUCCESS, userService.login(userLoginVo));
    }

}
